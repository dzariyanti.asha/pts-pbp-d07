# Tugas Kelompok PBP D-07
## Nama Anggota
- Bintang Gabriel Hutabarat - 2006596661
- Brandon Ivander - 2006535874
- Dzariyanti Asha Savitri - 2006596623
- Ekky Aliansyah - 2006596314
- Febi Imanuela - 2006596030
- Heidi Renata Halim - 2006596320
- Yudha Haris Permana - 2006535640
## Link Heroku
https://teman-covid.herokuapp.com/teman-covid/

## Cerita Aplikasi
Teman Covid adalah website yang dilatarbelakangi oleh pandemi COVID-19 yang masih melanda. Sebagaimana namanya, kami bermaksud membuat website yang dapat menjadi “teman” bagi masyarakat di tengah kondisi pandemi.
Melalui fitur untuk memberikan pengalaman penyintas COVID-19, memaparkan deskripsi obat-obatan, hingga membuat poster protokol kesehatan yang instan dan mudah diingat, kami berharap Teman Covid dapat mengayomi kebutuhan masyarakat dalam memperoleh informasi sekaligus mempromosikan awareness terkait protokol kesehatan. 
Semoga pengguna boleh beroleh manfaat dari aplikasi kami untuk menjaga kesehatan dirinya dan orang-orang di sekitarnya.


## Daftar Modul
1. Register & Login
    - Models: User
    - Form: UserForm untuk registrasi akun
2. Homepage
    - Navigasi ke menu Informasi Obat-obatan, Pengalaman Penyintas COVID-19, dan Wash Your Lyrics
    - Form: FeedbackForm untuk input kritik dan saran dari User
3. Informasi tentang obat-obatan
    - Models: Obat
    - Form: ObatForm untuk input Informasi Obat-obatan
4. Pengalaman penyintas COVID-19
    - Models: Review
    - Form: ReviewForm untuk input cerita Pengalaman Penyintas COVID-19
5. Wash Your Lyrics
    - Models: Lyrics
    - Form: LyricsForm untuk input lirik Wash Your Lyrics
6. Profil User
    - Models: User
    - Form: UserForm yang sama, tetapi untuk edit profil dari User
7. Comment Pengalaman
    - Models: Review
    - Form: CommentForm


## Persona
- Guest: Client yang belum memiliki akun pada website Teman Covid
- User: Client yang sudah memiliki akun pada website Teman Covid


